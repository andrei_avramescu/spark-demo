name := "Spark-Demo Project"

version := "0.1"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-graphx_2.10" % "1.5.1",
  "org.apache.spark" % "spark-streaming_2.10" % "1.5.1",
  "org.apache.spark" % "spark-sql_2.10" % "1.5.1",
  "org.apache.spark" % "spark-streaming-twitter_2.10" % "1.5.1",
  "org.apache.spark" % "spark-core_2.10" % "1.5.1",
  "com.databricks"   % "spark-csv_2.10" % "1.2.0",
)
