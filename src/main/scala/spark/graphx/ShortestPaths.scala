package spark.graphx

import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by andreiavramescu on 02/11/15.
 */

object ShortestPaths extends App {
  val maxDistance = 1000

  val sparkConf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("shortest-paths")

  val sc = new SparkContext(sparkConf)

  val verticesNumber = scala.util.Random.nextInt(100)
  val edgesSeq = for (a <- 0 to verticesNumber; b <- 0 to verticesNumber) yield Edge(a.toLong, b.toLong, scala.util.Random.nextInt(maxDistance).toLong)

  val vertices = sc.parallelize(Range(0, verticesNumber).map(it => (it.toLong, it)).toList)
  val edges: RDD[Edge[Long]] = sc.parallelize(edgesSeq)
  val initialGraph = Graph(vertices, edges).mapVertices((vId, _) => if (vId == 0) 0 else Long.MaxValue - maxDistance)

  val spGraph = initialGraph.pregel(Long.MaxValue)(
    (vertexId, current, newDist) => math.min(current, newDist),
    t =>
      if (t.srcAttr + t.attr < t.dstAttr)
        Iterator((t.dstId, t.srcAttr + t.attr))
      else
        Iterator.empty,
    (a, b) => math.min(a, b))

  initialGraph.edges.saveAsTextFile("./src/main/output/edges")

  spGraph.vertices.saveAsTextFile("./src/main/output/shortest-path")
}

