package spark.streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
 * Created by andreiavramescu on 03/11/15.
 */
object StreamingExample extends App {

  val conf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("streaming-example")
  val ssc = new StreamingContext(conf, Seconds(10))

  val files = ssc.fileStream("./src/main/resources/")

  files.foreachRDD(rdd => rdd.collect.foreach(println))

  ssc.start()

  files.start()

  ssc.awaitTermination()
}
