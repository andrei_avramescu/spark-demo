package spark.core

import org.apache.spark.sql.{Column, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}
import spark._

/**
 * Created by andreiavramescu on 02/11/2015.
 */

//  Word-count example on text from csv tweets archive
object WordCount extends App {

  val sparkConf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("word-count")

  val sc = new SparkContext(sparkConf)
  val sqlContext = new SQLContext(sc)

  // parse csv archive
  val df = sqlContext.read
    .format("com.databricks.spark.csv")
    .option("header", "true")
    .option("inferSchema", "true")
    .load("./src/main/resources/tweets.csv")
  val tweets = df.select(new Column("text")).map(row => row(0).toString)

  // split tweets in words
  val words = tweets.flatMap(text => text.split(" "))

  // calculate words number
  val (execTime, noWords) = executeWithTime(words.cache().count())
  val (execTimeInMem, noWordsInMem) = executeWithTime(words.count())

  println("Number of words tweeted : " + noWords + " in " + execTime / 1000000000.0 )
  println("Number of words tweeted from memory : " + noWordsInMem + " in " + execTimeInMem / 1000000000.0 )

  sc.stop()
}
