/**
 * Created by andreiavramescu on 02/11/15.
 */
package object spark {

  def executeWithTime[R](block: => R): (Long, R) = {

    val startTime = System.nanoTime()
    val result = block
    val endTime = System.nanoTime()

    (endTime - startTime, result)
  }
}
